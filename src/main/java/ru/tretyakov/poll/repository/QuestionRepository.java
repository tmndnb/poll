package ru.tretyakov.poll.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tretyakov.poll.entity.Question;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
}
