package ru.tretyakov.poll.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tretyakov.poll.entity.Poll;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PollRepository extends JpaRepository<Poll, Long> {
    List<Poll> findByNamePoll(String namePoll);

    List<Poll> findByDateStart(LocalDate dateStart);

    List<Poll> findByDateEnd(LocalDate dateEnd);

    List<Poll> findByActive(Boolean active);


}
