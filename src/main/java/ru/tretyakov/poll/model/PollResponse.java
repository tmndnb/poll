package ru.tretyakov.poll.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PollResponse extends BaseResponse {
    List<PollDTO> data;
}
