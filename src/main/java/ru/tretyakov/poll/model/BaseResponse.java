package ru.tretyakov.poll.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BaseResponse {
    private long recordsCount;
    private int pagesCount;
    private int page;
}
