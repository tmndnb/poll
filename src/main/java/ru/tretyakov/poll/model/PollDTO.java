package ru.tretyakov.poll.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.List;

@Data
@Accessors(chain = true)
public class PollDTO {
    private Long id;
    private String namePoll;
    private LocalDate dateStart;
    private LocalDate dateEnd;
    private Boolean active;
    private List<QuestionDTO> questionDTOS;
}
