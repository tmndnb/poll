package ru.tretyakov.poll.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class QuestionDTO {
    private Long id;
    private String text;
    private Integer displayOrder;
}
