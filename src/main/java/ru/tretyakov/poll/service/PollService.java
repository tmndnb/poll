package ru.tretyakov.poll.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.tretyakov.poll.entity.Poll;
import ru.tretyakov.poll.exeption.CustomException;
import ru.tretyakov.poll.model.PollDTO;
import ru.tretyakov.poll.model.PollResponse;
import ru.tretyakov.poll.repository.PollRepository;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class PollService {
    private final PollRepository pollRepository;
    private final QuestionService questionService;

    public PollResponse getAllPoll(Pageable pageable) {
        Page<Poll> pollList = pollRepository.findAll(pageable);
        PollResponse response = new PollResponse();
        response.setPage(pollList.getNumber());
        response.setPagesCount(pollList.getTotalPages());
        response.setRecordsCount(pollList.getTotalElements());
        response.setData(pollList.stream().map(this::convertToPollDTO).collect(Collectors.toList()));
        return response;
    }

    public PollDTO getOnePoll(Long id) {
        Poll poll = pollRepository.findById(id).orElseThrow(() ->
                new CustomException(String.format("Not Found, id=%s", id), HttpStatus.NOT_FOUND));
        return convertToPollDTO(poll);
    }

    public PollDTO savePoll(PollDTO pollDTO) {
        Poll poll = convertToPoll(pollDTO);
        Poll save = pollRepository.save(poll);
        poll.getQuestions().stream()
                .map(q -> q.setPoll(pollRepository.getOne(save.getId())))
                .map(questionService::save)
                .collect(Collectors.toList());
        return convertToPollDTO(save);
    }

    public void delete(Long id) {
        pollRepository.deleteById(id);
    }


    public List<PollDTO> findPaginated(int page, int size) {
        PageRequest pageRequest = PageRequest.of(page, size);
        Page<Poll> polls = pollRepository.findAll(pageRequest);
        return polls.stream().map(this::convertToPollDTO).collect(Collectors.toList());
    }

    private PollDTO convertToPollDTO(Poll poll) {
        return new PollDTO().setId(poll.getId())
                .setNamePoll(poll.getNamePoll())
                .setDateStart(poll.getDateStart())
                .setDateEnd(poll.getDateEnd())
                .setActive(poll.getActive())
                .setQuestionDTOS(poll.getQuestions().stream()
                        .map(questionService::convertToQuestResponse)
                        .collect(Collectors.toList()));
    }

    private Poll convertToPoll(PollDTO pollDTO) {
        return new Poll()
                .setId(pollDTO.getId())
                .setNamePoll(pollDTO.getNamePoll())
                .setDateStart(pollDTO.getDateStart())
                .setDateEnd(pollDTO.getDateEnd())
                .setActive(pollDTO.getActive())
                .setQuestions(pollDTO.getQuestionDTOS().stream()
                        .map(questionService::convertToQuestion)
                        .collect(Collectors.toList())
                );
    }
}
