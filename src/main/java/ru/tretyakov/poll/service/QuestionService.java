package ru.tretyakov.poll.service;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.tretyakov.poll.entity.Question;
import ru.tretyakov.poll.exeption.CustomException;
import ru.tretyakov.poll.model.QuestionDTO;
import ru.tretyakov.poll.repository.QuestionRepository;

@Service
@AllArgsConstructor
public class QuestionService {
    private final QuestionRepository questionRepository;

    public QuestionDTO getOne(Long id) {
        Question one = questionRepository.findById(id).orElseThrow(() ->
                new CustomException(String.format("Not Found, id=%s", id), HttpStatus.NOT_FOUND));
        return convertToQuestResponse(one);
    }

    public QuestionDTO save(Question question) {
        Question save = questionRepository.save(question);
        return convertToQuestResponse(save);
    }

    public void delete(Long id) {
        questionRepository.deleteById(id);
    }

    public QuestionDTO convertToQuestResponse(Question question) {
        return new QuestionDTO().setId(question.getId()).setText(question.getText()).setDisplayOrder(question.getDisplayOrder());
    }

    public Question convertToQuestion(QuestionDTO questionDTO) {
        return new Question()
                .setId(questionDTO.getId())
                .setText(questionDTO.getText())
                .setDisplayOrder(questionDTO.getDisplayOrder());
    }
}
