package ru.tretyakov.poll.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "poll")
@Accessors(chain = true)
@Data
public class Poll {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "name")
    private String namePoll;

    @Column(name = "date_start")
    private LocalDate dateStart;

    @Column(name = "date_end")
    private LocalDate dateEnd;

    @Column(name = "active")
    private Boolean active;

    @OneToMany(mappedBy = "poll",  cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Question> questions;


}
