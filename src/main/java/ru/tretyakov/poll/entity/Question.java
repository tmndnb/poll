package ru.tretyakov.poll.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "question")
@Accessors(chain = true)
@Data
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "text")
    private String text;

    @Column(name = "display_order")
    private Integer displayOrder;

    @ManyToOne
    @JoinColumn
    private Poll poll;

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", displayOrder=" + displayOrder +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Question question = (Question) o;
        return Objects.equals(id, question.id) &&
                Objects.equals(text, question.text) &&
                Objects.equals(displayOrder, question.displayOrder);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, displayOrder);
    }
}
