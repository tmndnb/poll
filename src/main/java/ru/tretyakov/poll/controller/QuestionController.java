package ru.tretyakov.poll.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tretyakov.poll.model.QuestionDTO;
import ru.tretyakov.poll.service.QuestionService;

@RestController
@AllArgsConstructor
@RequestMapping("question")
public class QuestionController {
    private final QuestionService questionService;

    @GetMapping("/{id}")
    public ResponseEntity<QuestionDTO> getOneQuestion(@PathVariable Long id) {
        return ResponseEntity.ok(questionService.getOne(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteQuestion(@PathVariable Long id) {
        questionService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
