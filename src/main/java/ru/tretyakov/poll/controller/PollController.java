package ru.tretyakov.poll.controller;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tretyakov.poll.model.PollDTO;
import ru.tretyakov.poll.model.PollResponse;
import ru.tretyakov.poll.service.PollService;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("poll")
public class PollController {
    private final PollService pollService;

    @GetMapping()
    public ResponseEntity<PollResponse> getAllPoll(Pageable pageable) {
        return ResponseEntity.ok(pollService.getAllPoll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PollDTO> getOnePoll(@PathVariable Long id) {
        return ResponseEntity.ok(pollService.getOnePoll(id));
    }

    @PostMapping()
    public ResponseEntity<PollDTO> addPoll(@RequestBody PollDTO request) {
        PollDTO pollNew = new PollDTO()
                .setNamePoll(request.getNamePoll())
                .setDateStart(request.getDateStart())
                .setDateEnd(request.getDateEnd())
                .setActive(request.getActive())
                .setQuestionDTOS(request.getQuestionDTOS());
        return ResponseEntity.ok(pollService.savePoll(pollNew));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePoll(@PathVariable Long id) {
        pollService.delete(id);
        return ResponseEntity.noContent().build();
    }


    @PutMapping()
    public ResponseEntity<Void> editPoll(@RequestBody PollDTO pollDTO) {
        PollDTO pollEdit = pollService.getOnePoll(pollDTO.getId());
        pollEdit.setNamePoll(pollDTO.getNamePoll())
                .setDateStart(pollDTO.getDateStart())
                .setDateEnd(pollDTO.getDateEnd())
                .setActive(pollDTO.getActive())
                .setQuestionDTOS(pollDTO.getQuestionDTOS());
        pollService.savePoll(pollEdit);
        return ResponseEntity.noContent().build();
    }


    @GetMapping("/search")
    public ResponseEntity<List<PollDTO>> search(@Param("page") int page, @Param("size") int size) {
        return ResponseEntity.ok(pollService.findPaginated(page, size));
    }

}
